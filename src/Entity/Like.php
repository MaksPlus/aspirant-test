<?php declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\Index;

/**
 * @ORM\Entity(repositoryClass="App\Repository\LikeRepository")
 * @ORM\Table(name="likes")})
 */
final class Like
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    private ?string $ip;

    /**

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Movie", inversedBy="likes")
     */
    private $movie;

    public function getMovie(): ?Movie
    {
        return $this->movie;
    }

    public function setMovie(?Movie $movie): self
    {
        $this->movie = $movie;

        return $this;
    }

    /**
     * @ORM\Column(type="datetime", name="create_at")
     */
    private ?\DateTime $create_at;

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId(?int $id): Like
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param string|null $ip
     */
    public function setIp(?string $ip): Like
    {
        $this->ip = $ip;
        return $this;
    }


    /**
     * @return \DateTime|null
     */
    public function getCreateAt(): ?\DateTime
    {
        return $this->create_at;
    }

    /**
     * @param \DateTime|null $create_at
     */
    public function setCreateAt(?\DateTime $create_at): Like
    {
        $this->create_at = $create_at;
        return $this;
    }


}
