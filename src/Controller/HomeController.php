<?php declare(strict_types=1);

namespace App\Controller;

use App\Entity\Like;
use App\Entity\Movie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpSpecializedException;
use Slim\Interfaces\RouteCollectorInterface;
use Twig\Environment;

class HomeController
{
    public function __construct(
        private RouteCollectorInterface $routeCollector,
        private Environment $twig,
        private EntityManagerInterface $em
    )
    {
    }

    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        try {
            $data = $this->twig->render('home/index.html.twig', [
                'trailers' => $this->fetchData(),
            ]);
        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);

        return $response;
    }

    protected function fetchData(): Collection
    {
        $data = $this->em->getRepository(Movie::class)
            ->findAll();

        return new ArrayCollection($data);
    }

    public function addLike(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        $id = $request->getAttribute('id');
        $trailer = $this->em->getRepository(Movie::class)
            ->find($id);
        if ($trailer) {
            $filter = [
                'ip'=>$request->getServerParams()['REMOTE_ADDR'],
                'movie'=>$trailer->getId()
            ];
            $hasLike = $this->em->getRepository(Like::class)->findOneBy($filter);
            if ($hasLike){
                $this->em->remove($hasLike);
                $this->em->flush();
            }else{
                $like = new Like();
                $like->setIp($request->getServerParams()['REMOTE_ADDR'])
                    ->setMovie($trailer)
                    ->setCreateAt(new \DateTime());
                $this->em->persist($like);
                $this->em->flush();
            }

            $payload = json_encode([
                'status' => 'success',
                'count' => $trailer->getLikes()->count(),
            ]);
        }

        $response->getBody()->write($payload);
        return $response
            ->withHeader('Content-Type', 'application/json');
    }

    public function show(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        try {
            $id = $request->getAttribute('id');

            $trailer = $this->em->getRepository(Movie::class)
                ->find($id);
            if ($trailer) {
                $data = $this->twig->render('home/detail.html.twig', [
                    'trailer' => $trailer,
                    'likes' => $trailer->getLikes()->count(),
                ]);
            } else {
                throw new EntityNotFoundException();
            }

        } catch (\Exception $e) {
            throw new HttpBadRequestException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);

        return $response;
    }
}
